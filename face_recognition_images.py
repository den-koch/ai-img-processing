import os
import pickle
import imutils
import cv2
from matplotlib import pyplot

from utils import face_encodings, nb_of_matches, face_rects

with open("encodings.pickle", "rb") as file:
    name_encodings_dict = pickle.load(file)

image_names = os.listdir("examples")

for image_name in image_names:

    image_path = os.path.sep.join(["examples", image_name])

    image = cv2.imread(image_path)
    image = imutils.resize(image, 1500)

    encodings = face_encodings(image)

    names = []

    for encoding in encodings:
        counts = {}

        for (name, encodings) in name_encodings_dict.items():
            counts[name] = nb_of_matches(encodings, encoding)

        if all(count == 0 for count in counts.values()):
            name = "Unknown"

        else:
            name = max(counts, key=counts.get)

        names.append(name)

    for rect, name in zip(face_rects(image), names):
        x1, y1, x2, y2 = rect.left(), rect.top(), rect.right(), rect.bottom()

        cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 3)

        text_width = cv2.getTextSize(name, cv2.FONT_HERSHEY_PLAIN, 3, 3)[0][0]
        cv2.putText(image, name, (x1 + ((x2 - x1) - text_width) // 2, y1 - 10),
                    cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)

    # cv2.imshow("Image", image)
    # cv2.waitKey(0)

    pyplot.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    pyplot.show()
